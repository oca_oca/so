#include "ConcurrentHashMap.h"


void* maximum_aux(void * args){
	struct_max maximum;
	maximum = *((struct_max*)args);
	while( indice <25){
		atomic<int> e = indice.getAndInc();
		pthread_mutex_lock(maximum->letras[e]);
		typename Lista< pair<string, unsigned int> >::Iterador itLista = maximum->tabla[e].CrearIt();
		if(itLista.HaySiguiente())
			pair<string, unsigned int > maxLocal(' ', 0);
		while( itLista.HaySiguiente()){
			maxLocal = new pair<string, unsigned int >;
			(*maxLocal) = itLista.Siguiente();
			if(get<1>(*maxLocal) > get<1>(*(maximum->maxGlobal)))

				//copiar maxLocal
				//free(maximum->maxGlobal.load())
				(*(maximum->maxGlobal)).store(maxLocal);
			itLista.Avanzar();
		}
		pthread_mutex_unlock(maximum->letras[e]);
	}
}


//ConcurrentHashMap()maxGlobal(){}

ConcurrentHashMap::ConcurrentHashMap(){
	maxGlobal.store(NULL);
	indice = 0;
	for(int i = 0; i < 26 ; i++){
		Lista<pair<string, unsigned int> >* a = new Lista< pair <string, unsigned int> >;
		tabla.push_back(*a);
		pthread_mutex_t m;
		int failure;
		failure = pthread_mutex_init(&m, NULL);
		if (failure) perror("no se pudo inicializar CHM (inicialización de mutex)");
		mutexDeLetras.push_back(m);
	// pusheo en cada posicion de la tabla, una lista vacia
	// 26 veces, una por cada letra del abecedario
	}
}


ConcurrentHashMap::~ConcurrentHashMap(){
//limpiar los tabla[i] pues están en mem dinámica

}


void ConcurrentHashMap::addAndInc(string key){
	uint entrada = hash(key);
	typename Lista< pair<string, unsigned int> >::Iterador itLista = tabla[entrada].CrearIt();
	bool existe = false;
	pthread_mutex_lock(&mutexDeLetras[entrada]);
	while( itLista.HaySiguiente() ){
		if( get<0>(itLista.Siguiente()) == key){
			get<1>(itLista.Siguiente()) += 1;
			existe = true;
			break;
		}
		itLista.Avanzar();
	} 
	if(!existe){
		pair<string, unsigned int> nuevo(key, 1);
		tabla[entrada].push_front(nuevo);
	} 
	pthread_mutex_unlock(&mutexDeLetras[entrada]);
}


bool ConcurrentHashMap::member(string key) {
	
	uint entrada = hash(key);
	typename Lista< pair<string, unsigned int> >::Iterador itLista = tabla[entrada].CrearIt();
	while( itLista.HaySiguiente() ){
		if( get<0>(itLista.Siguiente()) == key){
			return true;
		}
		itLista.Avanzar();
	} 
	return false;
}


pair<string, unsigned int> ConcurrentHashMap::maximum(unsigned int nt){
	//Vamos a usar mutexDeLetras para acceder a cada bucket de la tabla, para no pisarnos con la función addAndInc

	// Creo nt threads
	pthread_t ntThreads[nt];
	int i;
	for(i = 0; i < nt ; i++){
		pthread_create(&ntThreads[i], NULL, maximum_aux, this);
	}
	
	while(indice <26){
		// espero que termien todos los threads
	}
	indice = 0;

	return *maxGlobal ;
}




