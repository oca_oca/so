#ifndef CONCURRENT_HASH_MAP_H__
#define CONCURRENT_HASH_MAP_H__

#include <string> 
#include <atomic>
#include <vector>
#include <iostream>
#include <fstream>
#include <list>
#include <utility>  
#include <pthread.h>
#include "ListaAtomica.hpp"

using namespace std;

struct struct_max {
		atomic<int> indice;
		vector<pthread_mutex_t>* letras;	
		vector< Lista< pair<string, unsigned int> > >* tabla;
		atomic< pair<string, unsigned int> >* maxGlobal;
};


class ConcurrentHashMap {
private:

	vector< Lista< pair<string, unsigned int> > > tabla;
	vector<pthread_mutex_t> mutexDeLetras;
	atomic<int> indice;
	atomic< pair<string, unsigned int>* > maxGlobal;

	const uint hash(const string key) const {
		//toma un string(clave) y devuelve la posicion de la tabla donde corresponde ese string
		char primerLetra = key[0];
		uint entrada = (uint) primerLetra;
		entrada = entrada -97; // Revisar codigos ascii
		return entrada;
	}

	// para Ej 1 para el metodo maximum
	


public:
	// Constructor
	ConcurrentHashMap();

	// Destructor
	~ConcurrentHashMap();

	// agrega la clave sino  existe, suma 1 al significado si existe
	void addAndInc(string key);

	// member
	bool member(string key);

	//Pair
	pair<string, unsigned int> maximum(unsigned int nt);

	//static void* maximum_aux(void*);

	// static ConcurrentHashMap count_words(string arch);

	// static void *count_words_aux(void *args);

	// static ConcurrentHashMap count_words(list<string> archs);

	// static ConcurrentHashMap count_words(unsigned int n,list<string> archs);

	// static void *Procesar_Archivo(void *threadarg);
};

#endif /* CONCURRENT_HASH_MAP_H__*/
